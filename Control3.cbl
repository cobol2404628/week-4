       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONTROL3.
       AUTHOR. JULLADIT.

       
       
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 CITY-CODE               PIC   9 VALUE ZERO.
          88 CITY-IS-D                    VALUE 1.
          88 CITY-IS-L                    VALUE 2.
          88 CITY-IS-C                    VALUE 3.
          88 CITY-IS-G                    VALUE 4.
          88 CITY-IS-S                    VALUE 5.
          88 CITY-IS-W                    VALUE 6.
          88 UNIVERSITY-CITY              VALUE 1 THRU 4.
          88 CITY-CODE-NOT-VALID          VALUE 0, 7, 8, 9.

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter a city code (1-6) - " WITH NO ADVANCING 
           ACCEPT CITY-CODE 

           IF CITY-CODE-NOT-VALID THEN
              DISPLAY "Invalid city code ented"
           ELSE 
              IF CITY-IS-L THEN
                 DISPLAY "HEY, we're home."
              END-IF 
              IF CITY-IS-D THEN
                 DISPLAY "HEY, we're in the capital. "
              END-IF 
              IF UNIVERSITY-CITY THEN
                 DISPLAY "Apply the rent surcharge!"
              END-IF 
           END-IF
           SET CITY-CODE-NOT-VALID TO TRUE 
           DISPLAY CITY-CODE 
           GOBACK  
           .