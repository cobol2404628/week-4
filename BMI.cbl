       IDENTIFICATION DIVISION.
       PROGRAM-ID. BMICALC.
       AUTHOR. JULLADIT.
       
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 WEIGHT-IN-KG  PIC 9(5)V99.
       01 HEIGHT-IN-CM  PIC 9(3)V99.
       01 BMI           PIC 9(2)V99.
       01 CMTOM         PIC 9(5)V99.
       01 BMI-STATUS    PIC X(20).

       PROCEDURE DIVISION.
           DISPLAY "Enter your weight in kilograms: ".
           ACCEPT WEIGHT-IN-KG.

           DISPLAY "Enter your height in meters: ".
           ACCEPT HEIGHT-IN-CM.
           
           COMPUTE CMTOM = HEIGHT-IN-CM / 100
           COMPUTE BMI = WEIGHT-IN-KG /(CMTOM * CMTOM).

           IF BMI < 18.5
              MOVE "Underweight" TO BMI-STATUS
           ELSE
              IF BMI >= 18.5 AND BMI < 25
                 MOVE "Normal weight" TO BMI-STATUS
              ELSE
                 IF BMI >= 25 AND BMI < 30
                    MOVE "Overweight" TO BMI-STATUS
                 ELSE
                    MOVE "Obese" TO BMI-STATUS
                 END-IF.
      
           DISPLAY "Your BMI is: " BMI.
           DISPLAY "BMI Status: " BMI-STATUS.
      
           STOP RUN.